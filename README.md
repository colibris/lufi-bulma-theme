# Bulma theme for Lufi

Simple and clear theme for Lufi, with random background feature, based on Bulma.

## Dependencies

All dependencies are in the `public` folder, managed with npm.

- [Bulma start](https://github.com/jgthms/bulma-start) : just used for having a standard base
- [Bulma css framework](https://github.com/jgthms/bulma)
- [Bulma slider](https://github.com/Wikiki/bulma-slider)
- [Fork Awesome Icons](https://github.com/ForkAwesome/Fork-Awesome)
- [node-sass](https://github.com/sass/node-sass) to compile your own Sass file
- [postcss-cli](https://github.com/postcss/postcss-cli) and [autoprefixer](https://github.com/postcss/autoprefixer) to add support for older browsers
- [babel-cli](https://babeljs.io/docs/usage/cli/), [babel-preset-env](https://github.com/babel/babel-preset-env) and [babel-preset-es2015-ie](https://github.com/jmcriffey/babel-preset-es2015-ie) for compiling ES6 JavaScript files

## How to install this theme

- `cd /yourlufipath/themes`
- `git clone https://framagit.org/colibris/lufi-bulma-theme.git bulma`
- `cd bulma/public`
- `npm install`
- `npm deploy`
- edit `/yourlufipath/lufi.conf` and change theme value to bulma `theme => 'bulma',`
- restart lufi and smile (hopefully)

## How to customize this theme

The file `public/_sass/main.scss` contains most of the color variables, setup and custom css.
From the `public` folder, you can live watch changes and compile sass with the command `npm start`

## On the roadmap

- use vuejs and make components

## Authors and contributors

- Florian Schmitt <mrflos@lilo.org>